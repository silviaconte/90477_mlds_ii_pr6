# 90477_mlds_ii_PR6


## Name
Machine Learning Project 6.
An Application of K-means cluster analysis on data about COVID-19 in Indonesia.

## Description of the study
The aim of the study is to apply cluster analysis using the k-means algorithm. The dataset contains data about COVID-19 in Indonesia on 19 April 2020 from the Indonesian Task Force.

The dataset has 34 observations (the Indonesian provinces) and 4 variables: the name of the province, the number of confirmed cases, the number of recovered cases, and the number of deaths. Therefore, applying the k-means algorithm, the provinces are grouped according to confirmed, recovered, and death cases of COVID-19.

The results of this study can be used to implement different policies in provinces from different clusters by the Indonesian government.

## Available files:
- Scripts: contains python scripts and the jupyter notebook;
- Data: contains the input dataset;
- Results: contains the results; 
- References: contains the original paper; 
- Figures: contains images files.

# Machine Learning Project 6 - Application of K-means cluster analysis on data about COVID-19 in Indonesia
# Conte Silvia, Ragone Federica, Scozzari Sara


# 1. Data preparation
import pandas as pd      
import warnings          
warnings.filterwarnings("ignore")


# 1A. Upload data
data = pd.read_excel("data_covid_provinsi.xlsx", sheet_name="Sheet1") 
data.head()


# 1B. Descriptive statistics 
stat = data.describe().T[['count','mean', 'min', 'max', 'std']] 
stat.columns = ['Obs','Mean', 'Minimum', 'Maximum', 'Standard Deviation'] 
stat

stat.to_csv('Descriptive_Statistics.csv', sep=';', decimal=',')


# 1C. Barplot
import matplotlib.pyplot as plt   

plt.bar(data.index, data['Confirmed'], width=0.3, label='Confirmed cases')      
plt.bar(data.index+0.3, data['Recovered'], width=0.3, label='Recovered cases')  
plt.bar(data.index+0.6, data['Deaths'], width=0.3, label='Deaths')              
plt.title('Number of COVID-19 cases for each province')
plt.xlabel('Provinces')
plt.ylabel('Frequency')

positions = list(range(0,34))       
labels = data['Province'].tolist()  
plt.xticks(positions, labels, rotation=90)  

plt.legend()
plt.savefig('Barplot.png', dpi=300, bbox_inches='tight')
plt.show()


# 1D. Delete the province 'DKI Jakarta' from the dataset

# When there is a predominant data compared to the others, it is exlcuded from the analysis process.
# In our case Jakarta has more cases, so it is not included in the clustering process.
data_2 = data.drop(0)    
data_2.head()


# 1E. Missing values are deleted
data_2.isnull() # there are no missing values, so it is not necessary to use a function to remove them


# 1F. Standardization of the dataset 

# We want to standardized our dataset in order to make variables comparable.
from sklearn.preprocessing import StandardScaler

scale = StandardScaler()  # to create a StandardScaler object
data_scale = scale.fit_transform(data_2[["Confirmed","Recovered","Deaths"]]) 
data_scale = pd.DataFrame(data_scale, columns=[["Confirmed.sd","Recovered.sd","Deaths.sd"]]) 
data_scale.head()


# Since R (the software used for the original project) standardizes the variables 
# by dividing them with the root of their correct variance, 
# we need to correct our standardized values by multiplying them by the root of (n-1)/n.
import math  
data_scale_2 = data_scale * math.sqrt((32/33))
data_scale_2.head()

data_scale_2.to_csv('Standardized_Dataset.csv', sep=';', decimal=',')


# 2. Cluster analysis

# 2A. Clustering distances measurement using Euclidean distances 

# We compute the euclidean distance between each pair of observations
import numpy as np
from scipy.spatial.distance import pdist
from scipy.spatial.distance import squareform

euclidean_dist = pdist(data_scale_2, 'euclidean')  
square_euclidean_dist = squareform(euclidean_dist)
print(square_euclidean_dist)


import seaborn as sns

plt.figure(figsize=(7,5))
sns.heatmap(square_euclidean_dist, cmap='Blues', xticklabels=data['Province'], yticklabels=data['Province']) 
plt.savefig('Heatmap.png', dpi=300, bbox_inches='tight')
plt.show() 


# 2B. Determining the number of cluster (k) using: Elbow method, Silhouette method and Gap statistic method 

import random
random.seed(55)

from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score  


# Elbow method:
wcss = []   
for i in range(1, 16):
    km = KMeans(n_clusters=i, init='k-means++', max_iter=100, n_init=25, random_state=55).fit(data_scale_2) 
    wcss.append(km.inertia_)

plt.plot(range(1, 16), wcss)
plt.title('Elbow Method to find optimal number of clusters')
plt.xlabel('Number of clusters')
plt.ylabel('WCSS')
plt.savefig('Elbow_m.png', dpi=300, bbox_inches='tight')
plt.show()

# The optimal number of clusters, using the Elbow method, is k=2 


# Silhouette method:
scores = [0]   

for i in range(2,11):
    labels = KMeans(n_clusters=i, init="k-means++", random_state=55, max_iter=100, n_init=25).fit(data_scale_2).labels_  
    scores.append(silhouette_score(data_scale_2, labels, metric="euclidean", sample_size=33, random_state=55))
    
scores1 = pd.DataFrame({'Clusters' : range(1,11), 'Sil Score' : scores}) 
sns.lineplot(x = 'Clusters', y = 'Sil Score', data = scores1)            
plt.title('Silhouette Method to find optimal number of clusters')
plt.savefig('silh_m.png', dpi=300, bbox_inches='tight')
plt.show();

# The optimal number of clusters, using the Silhouette method, is k=2 


# Gap statistic method:
#!pip install gap-stat 
from gap_statistic import OptimalK      
optimalK = OptimalK(parallel_backend='None')

opt_clus = optimalK(data_scale_2, cluster_array=np.arange(1, 16))  
print('Optimal clusters: ', opt_clus)
#print(optimalK.gap_df)
#optimalK.plot_results()

plt.plot(optimalK.gap_df.n_clusters, optimalK.gap_df.gap_value)
plt.scatter(optimalK.gap_df[optimalK.gap_df.n_clusters == opt_clus].n_clusters,
            optimalK.gap_df[optimalK.gap_df.n_clusters == opt_clus].gap_value)   
plt.errorbar(range(1,16), optimalK.gap_df.gap_value, optimalK.gap_df.sk)

plt.xlabel('Cluster Count')
plt.ylabel('Gap Value')
plt.title('Gap statistic method to find optimal number of clusters')
plt.savefig('gap_stat_m.png', dpi=300, bbox_inches='tight')
plt.show()



# 2C. Representation of the clusters 

# Based on the results of the three methods, we can say that the optimal number of clusters is k=2.
labels = KMeans(n_clusters=2).fit(data_scale_2).labels_
df = pd.DataFrame({'Provinces': data.loc[1:, 'Province'], 'Cluster': labels+1})
df


from scipy.spatial import ConvexHull

X = np.array(data_scale_2)   
km = KMeans(n_clusters=2, init="k-means++", random_state=55, max_iter=100, n_init=25).fit(data_scale_2)
labels = km.labels_                 
ncluster = km.n_clusters             
centroids = km.cluster_centers_      

def drawclusters(ax):          
    for i in range(ncluster):  
        points = X[labels == i]  
        ax.scatter(x=points[:, 0], y=points[:, 1], c=col[i], label=f'Cluster {i + 1}') 
        hull = ConvexHull(points[:, [0,1]])                  
        vert = np.append(hull.vertices, hull.vertices[0])    
        ax.plot(points[vert, 0], points[vert, 1], '--', c=col[i])      
        ax.fill(points[vert, 0], points[vert, 1], c=col[i], alpha=0.2) 
    ax.scatter(centroids[:, 0], centroids[:, 1], c='red', label='Centroids', marker='x') 
    
col = ['blue', 'green']       
fig, ax = plt.subplots(1, figsize=(7, 5))  
drawclusters(ax)              
ax.legend()                   
plt.savefig('Clust_2.png', dpi=300, bbox_inches='tight')
plt.show()



# 3. Cluster Analysis with 'DKI Jakarta' 

# Going back to the dataset with all the observations, 
# we want to reproduce the analysis including the Jakarta observation.  

data.head()

scale = StandardScaler()
data_scale_j = scale.fit_transform(data[["Confirmed","Recovered","Deaths"]])  
data_scale_j = pd.DataFrame(data_scale_j, columns=[["Confirmed.sd","Recovered.sd","Deaths.sd"]])  

data_scale_j_2 = data_scale_j * math.sqrt((33/34))  
data_scale_j_2.head()

data_scale_j_2.to_csv('Standardized_Dataset_J.csv', sep=';', index=True, decimal=',')


# Using the Elbow method, we determine the optimal number of clusters:
wcss = []          
for i in range(1, 16):
    km = KMeans(n_clusters=i, init='k-means++', max_iter=100, n_init=25, random_state=55).fit(data_scale_j_2) 
    wcss.append(km.inertia_)

plt.plot(range(1, 16), wcss)
plt.title('Elbow Method to find optimal number of clusters')
plt.xlabel('Number of clusters')
plt.ylabel('WCSS')
plt.savefig('Elbow_m_2.png', dpi=300, bbox_inches='tight')
plt.show()

# Now, the optimal number of clusters seems to be 2 or 3; we try with k=3.

labels = KMeans(n_clusters=3).fit(data_scale_j_2).labels_
df = pd.DataFrame({'Provinces': data['Province'], 'Cluster': labels+1})
df

df.to_csv('Provinces_Clusters.csv', sep=';', index=True, decimal=',')


X = np.array(data_scale_j_2)   
km = KMeans(n_clusters=3, init="k-means++", random_state=55, max_iter=100, n_init=25).fit(data_scale_j_2)
labels = km.labels_                  
ncluster = km.n_clusters             
centroids = km.cluster_centers_      

def drawclusters(ax):
    for i in range(ncluster):
        points = X[labels == i]
        if(len(points)>=3):  
          ax.scatter(points[:, 0], points[:, 1], c=col[i], label=f'Cluster {i + 1}') 
          hull = ConvexHull(points[:, [0,1]])                
          vert = np.append(hull.vertices, hull.vertices[0])  
          ax.plot(points[vert, 0], points[vert, 1], '--', c=col[i])           
          ax.fill(points[vert, 0], points[vert, 1], c=col[i], alpha=0.2)       
        else:
          ax.scatter(points[:,0], points[:,1], c=col[i], label=f'Cluster {i + 1}')  
    ax.scatter(centroids[:, 0], centroids[:, 1], c='red', label='Centroids', marker='x')

col = ['blue', 'green', 'red']
fig, ax = plt.subplots(1, figsize=(5, 4))
drawclusters(ax)
ax.legend()
plt.savefig('Clust_3.png', dpi=300, bbox_inches='tight')
plt.show()

